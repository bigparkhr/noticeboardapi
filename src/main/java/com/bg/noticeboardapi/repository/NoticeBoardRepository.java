package com.bg.noticeboardapi.repository;

import com.bg.noticeboardapi.entity.Notice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeBoardRepository extends JpaRepository<Notice, Long> {
}
