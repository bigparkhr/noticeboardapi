package com.bg.noticeboardapi.service;

import com.bg.noticeboardapi.entity.Notice;
import com.bg.noticeboardapi.model.ArtChangeRequest;
import com.bg.noticeboardapi.model.NoticeItem;
import com.bg.noticeboardapi.model.NoticeRequest;
import com.bg.noticeboardapi.model.NoticeResponse;
import com.bg.noticeboardapi.repository.NoticeBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NoticeBoardService {
    private final NoticeBoardRepository noticeBoardRepository;

    public void setNotices(NoticeRequest request) {
        Notice addData = new Notice();
        addData.setLiterature(request.getLiterature());
        addData.setEconomicManagement(request.getEconomicManagement());
        addData.setArt(request.getArt());

        noticeBoardRepository.save(addData);
    }

    public List<NoticeItem> getNotice() {
        List<Notice> originList = noticeBoardRepository.findAll();

        List<NoticeItem> result = new LinkedList<>();

        for (Notice notice : originList) {
            NoticeItem addItem = new NoticeItem();
            addItem.setId(notice.getId());
            addItem.setLiterature(notice.getLiterature());
            addItem.setEconomicManagement(notice.getEconomicManagement());
            addItem.setArt(notice.getArt());

            result.add(addItem);

        }

        return result;
    }
    public NoticeResponse getNotice(long id) {
        Notice originData = noticeBoardRepository.findById(id).orElseThrow();
        NoticeResponse response = new NoticeResponse();

        response.setId(originData.getId());
        response.setLiterature(originData.getLiterature());
        response.setEconomicManagement(originData.getEconomicManagement());
        response.setArt(originData.getArt());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putArt(long id, ArtChangeRequest request){
        Notice originData = noticeBoardRepository.findById(id).orElseThrow();
        originData.setArt(request.getArt());

        noticeBoardRepository.save(originData);
    }

    public void delNotice(long id) {
        noticeBoardRepository.deleteById(id);
    }
}
