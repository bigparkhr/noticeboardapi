package com.bg.noticeboardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeItem {
    private Long id;
    private String Literature;
    private String EconomicManagement;
    private String Art;
}
