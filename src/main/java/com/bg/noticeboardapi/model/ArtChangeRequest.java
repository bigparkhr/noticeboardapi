package com.bg.noticeboardapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArtChangeRequest {
    @Column(nullable = false)
    private String Art;
}
