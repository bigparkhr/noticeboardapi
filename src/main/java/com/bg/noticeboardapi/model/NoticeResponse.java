package com.bg.noticeboardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeResponse {
    private Long id;
    private String Literature;
    private String EconomicManagement;
    private String Art;
    private String etcMemo;
}
