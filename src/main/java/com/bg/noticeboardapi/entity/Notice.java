package com.bg.noticeboardapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Notice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String Literature;

    @Column(nullable = false)
    private String EconomicManagement;

    @Column(nullable = false)
    private String Art;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
