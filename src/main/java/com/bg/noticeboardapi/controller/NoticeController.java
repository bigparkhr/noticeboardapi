package com.bg.noticeboardapi.controller;

import com.bg.noticeboardapi.model.ArtChangeRequest;
import com.bg.noticeboardapi.model.NoticeItem;
import com.bg.noticeboardapi.model.NoticeRequest;
import com.bg.noticeboardapi.model.NoticeResponse;
import com.bg.noticeboardapi.service.NoticeBoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/notice")
public class NoticeController {
    private final NoticeBoardService noticeBoardService;

    @PostMapping("/people")
    public String setNotice(@RequestBody NoticeRequest request) {
        noticeBoardService.setNotices(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<NoticeItem> getNotices() { return noticeBoardService.getNotice(); }

    @GetMapping("/detail/{id}")
    public NoticeResponse getNotice(@PathVariable long id) { return noticeBoardService.getNotice(id); }

    @PutMapping("/art/{id}")
    public String putArt(@PathVariable long id, @RequestBody ArtChangeRequest request) {
        noticeBoardService.putArt(id, request);

        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delNotice(@PathVariable long id) { noticeBoardService.delNotice(id); return "OK"; }
}
